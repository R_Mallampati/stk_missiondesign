function totalScore = score(tripOrder,planeDirection,pos,turningLimit)
    
    currentPos = pos(:,1);
    currentDirection = planeDirection;
    cumulatedAngle = 0;
    cumulatedDistance = 0;
    
    for j = 2:numel(tripOrder)-1
        nextPos = pos(:,tripOrder(j));
%         xk = nextPos(1);
%         yk = nextPos(2);
% 
%         xp = currentPos(1);
%         yp = currentPos(2);
% 
%         a = -currentDirection(2);
%         b = currentDirection(1);
% 
%         dif = (norm(currentPos)^2-norm(nextPos)^2)/2;
% 
%         xCenter = (dif+(yp-yk)*(b/a*xp+yp))/(xp-xk+b/a*(yp-yk));
%         yCenter = b/a*(xCenter-xp)+yp;

%         R = sqrt((xp-xCenter)^2+(yp-yCenter)^2);
        R = TurningRadius(currentPos,currentDirection,nextPos);
        angle = pi - 2*acos(norm(nextPos-currentPos)/(2*R));
        if R < turningLimit
            R = Inf;
            cumulatedDistance = Inf;
            cumulatedAngle = Inf;
            break
        end
        cumulatedAngle = angle + cumulatedAngle;
        cumulatedDistance = angle*R + cumulatedDistance;
        
        currentPos = nextPos;
        currentDirection = [currentDirection(1)*cos(angle) - currentDirection(2)*sin(angle); currentDirection(1)*sin(angle) + currentDirection(2)*cos(angle)];
    end
    totalScore = [cumulatedAngle cumulatedDistance];
end