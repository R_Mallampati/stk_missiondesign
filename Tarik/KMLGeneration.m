%% KML Generation
% Author: Tarik Errabih
% Acknowledgment: Eric Ogier
% Description: This code generate KML files of the GPS reflections at regular time intervals
% specified by the user. Settings have the same meaning as in the main
% program GPSReflections.m. Additionnally, the user specifies the output folder, time step and start and
% end date.

clear
cd(fileparts(which(mfilename))); addpath('./Functions');

%% Settings
KMLpath = './KML/';                    % Output folder

% Start date
year = 2020; month = 11; day = 02;
hourStart = 11; minuteStart = 0; secondStart = 00; % Sydney daylight savings time zone (UTC+11), 24 hour format

% End date (on the same day)
hourEnd = 20; minuteEnd = 00; secondEnd = 00;      % Sydney daylight savings time zone (UTC+11), 24 hour format

% Time between each KML file
tStep = 10;                            % minutes

% Other settings
airplaneAlt = 1500;                    % meters
shipPosLLA = [-34.41009, 150.9877, 0]; % [°,°,m]
ElevationMaskAngle = 45;               %Elevation Treshold for visibility in deg
N = 10;                                %Number of reflections to be calculated per satellite

%Almanach
UpdateAlmanach = 0; %1 to retrieve the latest almanach online, 0 to use the specified file
almanachsPath = './Almanachs/';
almanachFilename = '17-Nov-2020.txt';

%% Initialisation
% Creation of a time array
startDate = datetime(year, month, day, hourStart, minuteStart, secondStart);
startDate = startDate - 11*3600/86400; %Conversion to UTC
tStart = getGPStime(startDate);

endDate = datetime(year, month, day, hourEnd, minuteEnd, secondEnd);
endDate = endDate - 11*3600/86400; %Conversion to UTC
tEnd = getGPStime(endDate);

tRange = [tStart:60*tStep:tEnd];
numMaps = numel(tRange);

% Initialisation of the GPS constellation
wgs84 = wgs84Ellipsoid;

if UpdateAlmanach;
    URL = ['https://www.navcen.uscg.gov/?pageName=currentAlmanac&format=yuma-txt'];
    almanachFilename = strcat(almanachsPath,date,'.txt');
    urlwrite(URL,almanachFilename);
else
    almanachFilename = strcat(almanachsPath,almanachFilename);
end

GPS = Constellation();
GPS.set('ReferencePositionGeodetic',shipPosLLA,'ElevationMaskAngle',ElevationMaskAngle);
GPS.readAlmanac(almanachFilename);
clear ans

%% Loop
numLoop = 0;

for t = tRange
    dateFile = datestr(datetime(year, month, day, hourStart, minuteStart + numLoop*tStep, secondStart),30);
    numLoop = numLoop +1;
    %% Position and visibility determination
    OrbitalParameters = GPS.calculateCoordinates(t, 1:32);

    Visibility = GPS.calculateVisibility(OrbitalParameters);
    VisibilityIndicator = [Visibility.Indicator];
    VisibleSVs = nonzeros([1:32].*VisibilityIndicator)';

    AER = [Visibility.AER];
    SVelevations = AER(2:3:end);
    SVazimuths = AER(1:3:end);

    numSat = sum(VisibilityIndicator);
    reflections = zeros(N+1, 3, numSat);

    k = 1;
    for i = VisibleSVs
        for j = 0:N
            shipAngle = j*pi/N;
            reflectionLocal = local_dihedral_reflection(SVelevations(i), SVazimuths(i), airplaneAlt, shipAngle);
            [lat,lon,alt]= enu2geodetic(reflectionLocal(1), reflectionLocal(2), reflectionLocal(3), shipPosLLA(1), shipPosLLA(2), shipPosLLA(3), wgs84);
            reflections(j+1,:,k) = [lat; lon; alt];
        end
        k = k + 1;
    end


%% KML file generation
pointLabels = strings(1,numSat*(N+1));
KMLfilename = strcat(KMLpath,dateFile);
kmlwritepoint(KMLfilename, reflections(:,1,:), reflections(:,2,:), reflections(:,3,:),'Name',pointLabels);
end