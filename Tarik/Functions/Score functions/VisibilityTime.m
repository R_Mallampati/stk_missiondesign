function tRemaining = VisibilityTime(tStart, tStep, PRN, ElevationMaskAngle, constellation)
    OrbitalParameters = constellation.calculateCoordinates(tStart, 1:32);

    Visibility = constellation.calculateVisibility(OrbitalParameters);
    VisibilityIndicator = [Visibility.Indicator];
    tRemaining = tStart;
    
    while VisibilityIndicator(PRN)
        tRemaining = tRemaining+tStep;
        OrbitalParameters = constellation.calculateCoordinates(tRemaining, 1:32);
        Visibility = constellation.calculateVisibility(OrbitalParameters);
        VisibilityIndicator = [Visibility.Indicator];
    end
end