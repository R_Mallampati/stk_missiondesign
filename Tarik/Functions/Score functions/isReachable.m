function reachable = isReachable(R, minTurnRadius, time2reach, visTime)
    if R<minTurnRadius || visTime<time2reach
        reachable = 0;
    else
        reachable = 1;
    end
end