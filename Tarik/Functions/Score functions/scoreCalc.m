function score = scoreCalc(turnRad,timeRemaining,timeReach,reachable,satElevation)
    if reachable
        score = turnRad*satElevation/(timeReach*timeRemaining);
    else
        score = 0;
    end
end