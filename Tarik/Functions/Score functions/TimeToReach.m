%need angle of circle, then just calculate length of arc, then divide by
%speed

function t = TimeToReach(planePos,keypointPos,centerPos, radius, speed)
    angle = abs(acos(dot(planePos-centerPos,keypointPos'-centerPos)/(norm(planePos-centerPos)*norm(keypointPos'-centerPos))));
    distance = 1000*radius*angle;
    t = distance/speed;
end