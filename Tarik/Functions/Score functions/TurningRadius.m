function [R,centerPos] = TurningRadius(planePos,planeDirection,keypointPos)
    xk = keypointPos(1);
    yk = keypointPos(2);
    
    xp = planePos(1);
    yp = planePos(2);
    
    a = -planeDirection(2);
    b = planeDirection(1);
    orthogonalDirection = [a;b];
%     dif = (norm(planePos)^2-norm(keypointPos)^2)/2;
%     
%     xCenter = (dif+(yp-yk)*(b/a*xp+yp))/(xp-xk+b/a*(yp-yk));
%     yCenter = b/a*(xCenter-xp)+yp;
%     
%     centerPos = [xCenter, yCenter];
%     
%     R = sqrt((xp-xCenter)^2+(yp-yCenter)^2);%/1000;
    
    t = norm(keypointPos-planePos)^2/(2*dot(keypointPos-planePos,orthogonalDirection));
    centerPos = planePos + t*orthogonalDirection;
    R = sqrt((xp-centerPos(1))^2+(yp-centerPos(2))^2);
end
    