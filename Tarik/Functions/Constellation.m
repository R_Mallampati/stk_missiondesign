%% GPS - Constellation
%  Version : 1.0.0.3
%  Author  : E. Ogier
%  Release : Jan. 15 2019

classdef Constellation < matlab.mixin.SetGet % ('hgsetget' for previous versions)
    
    % Properties (public access)
    properties (Access = 'public')
        
        % Almanac file 
        File = 'current.txt';
        
        % GPS-UTC time leap seconds
        LeapSeconds = +18;
        
        % Reference position in geodetic frame [�][�][m]
        ReferencePositionGeodetic = NaN(1,3)
        
        % Reference position in geodetic frame, litteral [�][�][m]
        ReferencePositionGeodeticLitteral = {'','',''};
        
        % Reference position in ECEF frame [m][m][m]
        ReferencePositionECEF = NaN(1,3)
        
        % Elevation mask angle [�]
        ElevationMaskAngle = 5;
        
    end
    
    % Properties (private access for set)
    properties (SetAccess = 'private')
        
        % Almanac
        Almanac = ...                                           % GPS constellation almanac:
               repmat(struct('ID',         NaN,...              % - satellite                        (PRN)
                             'Health',     1,  ...              % - health                           (healthy: 0)
                             'e',          NaN,...              % - eccentricity
                             't0',         NaN,...              % - time of applicability            [s]
                             'i',          NaN,...              % - orbital inclination              [rad]
                             'OMEGAdot',   NaN,...              % - rate of right ascension          [rad/s]
                             'sqrtA',      NaN,...              % - square root of semi-major axis   [m^0.5]
                             'OMEGA0',     NaN,...              % - right ascension at week          [rad]
                             'omega',      NaN,...              % - argument of perigee              [rad]
                             'M0',         NaN,...              % - mean anomaly
                             'Af0',        NaN,...              % - clock parameter 0                [s]
                             'Af1',        NaN,...              % - clock parameter 1                [s/s]
                             'Week',       NaN),1,32);          % - week
  
        % Periods
        Periods = zeros(1,32);
        
        % Variations of longitudes by period
        VariationsLongitudes = zeros(1,32);
                
    end
    
    % Properties (private access)
    properties (Constant)
        
        % Earth constants
        Constants = ...
            struct('Earth',...                                  % Earth:
                    struct('Radius',     6.378e6,...            % - radius                           [m]
                           'OMEGAdot',   7.2921151467e-5,...    % - rotation rate                    [rad/s]
                           'mu',         3.986004418e14),...    % - standard gravitational parameter [m^3/s^2]
                   'Mathematics',...                            % Mathematics:                   
                    struct('rad2deg',    180/pi,...             % - radian to degree conversion factor
                           'deg2rad',    pi/180));              % - degree to radian conversion factor
                       
        % GPS reference ellipsoid
        Ellipsoid = wgs84Ellipsoid('meters');                   % WGS84 ellipsoid
        
        % GPS reference date
        ReferenceDate = ...                                     % 06th january 1980
                    datetime('06/01/1980',...
                             'InputFormat', 'dd/MM/yyyy',...
                             'TimeZone',    'UTC');
        
    end
    
    % Methods
    methods (Access = 'public')
        
        % Constructor
        function Object = Constellation(varargin)
            
            for v = 1:2:length(varargin)
                
                Property = varargin{v};
                Value = varargin{v+1};
                set(Object,Property,Value);
                
            end
            
        end
        
        % Method 'set'
        function Object = set(Object,varargin)
                     
            Properties = varargin(1:2:end);
            Values     = varargin(2:2:end);
            
            for n = 1:length(Properties)
                
                if isprop(Object,Properties{n})
                    
                    switch lower(Properties{n})
                      
                        case lower('ReferencePositionGeodetic')
                            
                            % Reference position in geodetic frame [�][�][m]
                            Object.ReferencePositionGeodetic = Values{n};                        
                            
                            % Reference position in geodetic frame, litteral [�][�][m] 
                            Object.ReferencePositionGeodeticLitteral = Conversion(Values{n});
                            
                            % Reference position in ECEF frame [m][m][m]
                            [x,y,z]= geodetic2ecef(Object.Ellipsoid,Values{n}(1),Values{n}(2),Values{n}(3));  
                            Object.ReferencePositionECEF = [x y z];
                            
                        case lower('ReferencePositionECEF')
                            
                            % Reference position in ECEF frame [m][m][m]
                            Object.ReferencePositionECEF = Values{n};
                            
                            % Reference position in geodetic frame [�][�][m]
                            [l,g,h] = ecef2geodetic(Values{n}(1),Values{n}(2),Values{n}(3),Object.Ellipsoid);   
                            Object.ReferencePositionGeodetic = [l g h];
                            
                            % Reference position in geodetic frame, litteral [�][�][m] 
                            Object.ReferencePositionGeodeticLitteral = Conversion(Values{n});
                            
                        otherwise
                            
                            Object.(Properties{n}) = Values{n};
                    
                    end
                    
                end
                
            end
            
            % Conversion
            function LGH = Conversion(lgh)
                
                % Latitude
                switch sign(Values{n}(1))
                    case +1, NS = 'N';
                    case  0, NS = ' ';
                    case -1, NS = 'S';
                end
                DMS = Deg2DMS(lgh(1));
                LGH{1} = sprintf('%02u�%02u''%06.3f" %c',DMS(1),DMS(2),DMS(3),NS);
                
                % Longitude
                switch sign(Values{n}(2))
                    case +1, EW = 'E';
                    case  0, EW = ' ';
                    case -1, EW = 'W';
                end
                DMS = Deg2DMS(lgh(2));
                LGH{2} = sprintf('%03u�%02u''%06.3f" %c',DMS(1),DMS(2),DMS(3),EW);
                
                % Altitude
                LGH{3} = sprintf('%.2fm',lgh(3));
                
                % Conversion from degrees to DMS format
                function DMS = Deg2DMS(lg)
                    
                    lg  = abs(lg);
                    lgm = 60*rem(lg,1);
                    DMS = [fix(lg),fix(lgm),60*rem(lgm,1)];
                    
                end
                
            end
            
        end
        
        % Method 'get'
        function Value = get(varargin)
            
            if nargin == 1
                disp(varargin{1});
                return
            end
            
            Object = varargin{1};
            Property = varargin{2};
            
            if isprop(Object,Property)
                Value = Object.(Property);
            else
                error('Property "%s" not supported !',Property);
            end
            
        end
        
        % Method 'readAlmanac'
        function Object = readAlmanac(Object,File)
            
            % Almanc file
            if ~nargin
                File = Object.File;
            else
                Object.File = File;
            end            
            if isempty(File)
                error('No almanac file !');
            end            
            
            Fields = fieldnames(Object.Almanac);
            for n = 1:32
                for f = 1:numel(Fields)
                    Object.Almanac(n).(Fields{f}) = NaN;
                end
            end
            
            FID = fopen(File);
            
            % Format detection
            Data = fscanf(FID,'%s\n',1);
            switch Data
                
                % YUMA
                case '********'
                    Object = ReadingYUMA(Object,FID);
                                    
                % SEM    
                otherwise
                    if isnumeric(str2double(Data))
                        Object = ReadingSEM(Object,FID);
                    else
                        error('Unknown file format');
                    end
                    
            end
            
            fclose(FID);
            
            % Sidereal periods
            for s = 1:32
                Object.Periods(s) = 4*pi/(sqrt(Object.Constants.Earth.mu)/Object.Almanac(s).sqrtA^3);    
            end
            
            % Variations of longitudes by ground track periods
            Coordinates = Object.calculateCoordinates([0 Object.Periods(s)],1:32);
            for s = 1:32
                Object.VariationsLongitudes(s) = diff(Coordinates(s).GEO(:,2));
            end            
            
            % Reading of YUMA-file
            function Object = ReadingYUMA(Object,FID)
                
                % Almanac lines reading
                while ~feof(FID)
                    
                    Data = textscan(FID,'%s %f\n','delimiter',':');
                    if isempty(Data{1})
                        break
                    end
                    switch Data{1}{1}
                        case 'ID',         ID = Data{2}; Object.Almanac(ID).ID       = Data{2};
                        case 'Health',                   Object.Almanac(ID).Health   = Data{2};
                        case 'Eccentricity',             Object.Almanac(ID).e        = Data{2};
                        case 'Time of Applicability(s)', Object.Almanac(ID).t0       = Data{2};
                        case 'Orbital Inclination(rad)', Object.Almanac(ID).i        = Data{2};
                        case 'Rate of Right Ascen(r/s)', Object.Almanac(ID).OMEGAdot = Data{2};
                        case 'SQRT(A)  (m 1/2)',         Object.Almanac(ID).sqrtA    = Data{2};
                        case 'Right Ascen at Week(rad)', Object.Almanac(ID).OMEGA0   = Data{2};
                        case 'Argument of Perigee(rad)', Object.Almanac(ID).omega    = Data{2};
                        case 'Mean Anom(rad)',           Object.Almanac(ID).M0       = Data{2};
                        case 'Af0(s)',                   Object.Almanac(ID).Af0      = Data{2};
                        case 'Af1(s/s)',                 Object.Almanac(ID).Af1      = Data{2};
                        case 'week',                     Object.Almanac(ID).Week     = Data{2};
                    end
                    
                end                
                
            end
            
            % Reading of SEM-file
            function Object = ReadingSEM(Object,FID)
                
                fgetl(FID);
                
                Week = fscanf(FID,'%f',1);
                t0   = fscanf(FID,'%f',1);
                
                % Almanac lines reading
                while ~feof(FID)
                    
                    ID = fscanf(FID,'%f',1);   
                    if isempty(ID)
                        break
                    end
                    
                    SVN = fscanf(FID,'%f',1); %#ok<NASGU>
                    URA = fscanf(FID,'%f',1); %#ok<NASGU>
                    
                    Object.Almanac(ID).ID       = ID;
                    
                    Object.Almanac(ID).e        = fscanf(FID,'%f',1);                           
                    Object.Almanac(ID).i        = (0.3+fscanf(FID,'%f',1))*pi;  % [semi-circle] to [rad]                    
                    Object.Almanac(ID).OMEGAdot = fscanf(FID,'%f',1)*pi;        % [semi-circle] to [rad]
                    
                    Object.Almanac(ID).sqrtA    = fscanf(FID,'%f',1);    
                    Object.Almanac(ID).OMEGA0   = fscanf(FID,'%f',1)*pi;        % [semi-circle] to [rad]                    
                    Object.Almanac(ID).omega    = fscanf(FID,'%f',1)*pi;        % [semi-circle] to [rad]  
                    
                    Object.Almanac(ID).M0       = fscanf(FID,'%f',1)*pi;        % [semi-circle] to [rad]  
                    Object.Almanac(ID).Af0      = fscanf(FID,'%f',1);     
                    Object.Almanac(ID).Af1      = fscanf(FID,'%f',1);  
                    
                    Object.Almanac(ID).Health   = fscanf(FID,'%f',1);
                    Satellite_configuration     = fscanf(FID,'%f',1); %#ok<NASGU>
                    
                    Object.Almanac(ID).Week     = Week;
                    Object.Almanac(ID).t0       = t0;
                    
                end
                
            end
            
        end
        
        % Method 'calculateOrbitalPlanes'
        function OrbitalPlanes = calculateOrbitalPlanes(Object)
                        
            % Orbital planes
            OrbitalPlanes = cell(6,1);
            
            % Right ascension of the ascending node
            RAAN = [Object.Almanac(:).OMEGA0];
            
            % Distance metric
            Metric = @(x,y)abs(atan2(sin(x-y),cos(x-y)));
            
            % Distance
            D = pdist(RAAN',Metric);
            
            % Distance matrix
            D2 = squareform(D);
            D2(:,isnan(RAAN)) = 1;
            D2(isnan(RAAN),:) = 1;
            
            % Number of orbital planes
            No = numel(Object.OrbitalPlanes);
            
            % Number of clusters, including missing satellites
            Nc = No;
            if any(isnan(RAAN))
                Nc = Nc+1;
            end
            
            % Linkage in terms of right ascension
            T = linkage(D2);
            
            % Cluster indices
            Ic = cluster(T,'maxclust',Nc);
            
            % Orbital planes
            for no = 1:No
                OrbitalPlanes{no} = sort(find(eq(Ic,no)));
            end
            
        end
                
        % Method 'getTime'
        function [t, d, w, r] = getTime(Object,Offset)
            
            switch nargin
                case 1, Offset = 0;
            end
            
            % Current UTC time
            T = datetime('now','TimeZone','UTC');
            
            % Adding of leap seconds
            T = T+(Object.LeapSeconds+Offset)/86400;
            
            % Current date
            DV = datevec(T);
            
            % Day number
            d = day(T,'dayofweek');
            
            % GPS time of week [s]
            t = 86400*(d-1)+[3600 60 1]*DV(4:6)';
            
            % Elapsed time from GPS reference date [day]
            et = days(T-Object.ReferenceDate);
            
            % Rollover number
            r = floor(et/7/1024);
            
            % Week number
            w = floor(et/7-r*1024);
            
        end
        
        % Method 'calculateCoordinates' (ECEF, ECI and geodetic coordinates)
        function Coordinates = calculateCoordinates(Object,varargin)
            
            % Arguments
            switch nargin                
                case 1                    
                    T = Object.getTime();
                    SV = 1:32;                    
                case 2                    
                    T = Object.getTime();
                    SV = varargin{1};                    
                case 3
                    T = varargin{1};
                    SV = varargin{2};                    
            end
            
            % GPS time
            if isempty(T)
                T = Object.getGPSTime();
            end
            
            % SV
            if isempty(SV)
                SV = 1:32;
            end
            
            % Number of iterations
            K = numel(T);
            
            % Constellation structure preallocation
            Coordinates = ...
                repmat(struct('ECEF',                     nan(K,3),...
                              'ECI',                      nan(K,3),...
                              'GEO',                      nan(K,3),...
                              'Eccentricity',             NaN,...
                              'SemimajorAxis',            NaN,...
                              'Inclination',              NaN,...
                              'LongitudeOfAscendingNode', NaN,...
                              'ArgumentOfPeriapsis',      NaN,...
                              'TrueAnomaly',              NaN,...
                              'ArgumentOfLatitude',       NaN),...
                       1,32);
            
            % Newton method for eccentricity anomaly
            Ekf = @(E,e,Mk) E-((E-e*(sin(E))-Mk)/(1-e*(cos(E))));
            
            % Satellite position computation
            for s = SV
                
                if Object.Almanac(s).ID == s
                    
                    % Almanac epoch
                    t0 = Object.Almanac(s).t0;
                    
                    for k = 1:K
                        
                        % Time from epoch
                        tk = T(k)-t0;

                        % Mean anomaly at epoch
                        M0 = Object.Almanac(s).M0;
                        
                        % Semi-major axis
                        A = Object.Almanac(s).sqrtA^2;
                        
                        % Mean motion
                        n = sqrt(Object.Constants.Earth.mu)/Object.Almanac(s).sqrtA^3;
                        
                        % Mean anomaly
                        Mk = M0+n*tk;
                        
                        % Eccentricty
                        e = Object.Almanac(s).e;
                        
                        % Eccentricity anomaly
                        Ekp = inf;
                        Ek = Ekf(Mk,e,Mk);
                        while abs(Ek-Ekp) > 1e-10
                            Ekp = Ek;
                            Ek = Ekf(Ek,e,Mk);
                        end
                        
                        % True anomaly
                        nuk = atan2(sqrt(1-e^2)*sin(Ek),cos(Ek)-e);
                        
                        % Argument of perigee
                        omega = Object.Almanac(s).omega;
                        
                        % Argument of latitude
                        phik = omega+nuk;
                        
                        % Radius
                        rk = A*(1-e*cos(Ek));
                        
                        % Orbit inclination
                        i = Object.Almanac(s).i;
                        
                        % Positions in orbital plane
                        xkp = rk*cos(phik);
                        ykp = rk*sin(phik);
                        
                        % Longitude of ascending node
                        OMEGAk = Object.Almanac(s).OMEGA0+(Object.Almanac(s).OMEGAdot-Object.Constants.Earth.OMEGAdot)*tk-Object.Constants.Earth.OMEGAdot*t0;
                        
                        % Orbital elements
                        Coordinates(s).Eccentricity             = e;
                        Coordinates(s).SemimajorAxis            = A;
                        Coordinates(s).Inclination              = i;
                        Coordinates(s).LongitudeOfAscendingNode = OMEGAk;
                        Coordinates(s).ArgumentOfPeriapsis      = omega;
                        Coordinates(s).TrueAnomaly              = nuk;  
                        Coordinates(s).ArgumentOfLatitude       = phik;   % Additional element                  
                        
                        % ECEF X-axis, Y-axis and Z-axis coordinates
                        xk = xkp*cos(OMEGAk)-ykp*cos(i)*sin(OMEGAk);
                        yk = xkp*sin(OMEGAk)+ykp*cos(i)*cos(OMEGAk);
                        zk = ykp*sin(i);
                                                
                        % Geodetic latitude, longitude and altitude
                        [l,g,h] = ecef2geodetic(xk,yk,zk,Object.Ellipsoid);
                        
                        % ECEF coordinates
                        Coordinates(s).ECEF(k,1) = xk;
                        Coordinates(s).ECEF(k,2) = yk;
                        Coordinates(s).ECEF(k,3) = zk;
                        
                        % ECI coordinates
                        phie = Object.Constants.Earth.OMEGAdot*tk;
                        Coordinates(s).ECI(k,1) = xk*cos(phie)-yk*sin(phie);
                        Coordinates(s).ECI(k,2) = xk*sin(phie)+yk*cos(phie);
                        Coordinates(s).ECI(k,3) = zk;
                        
                        % Geodetic coordinates
                        Coordinates(s).GEO(k,1) = Object.Constants.Mathematics.rad2deg*l;
                        Coordinates(s).GEO(k,2) = Object.Constants.Mathematics.rad2deg*g;
                        Coordinates(s).GEO(k,3) = h;
                                                
                    end
                    
                end
                
            end
           
        end
        
        % Method 'calculateVisibility' (AER coordinates and visibility indicators)
        function Visibility = calculateVisibility(Object,Coordinates)
            
            % Number of iterations
            K = size(Coordinates(1).GEO,1);
            
            % Number of satellites
            S = numel(Coordinates);
            
            % Coordinates structure preallocation
            Visibility = ...
                repmat(struct('AER',       nan(K,3),...
                              'Indicator', false(K,1)),...
                       1,S);
            
            % Satellite position computation
            for s = 1:S
                
                if Object.Almanac(s).ID == s
                    
                    for k = 1:K
                        
                        % Local spherical azimuth, elevation and range
                        [a,e,r] = ...
                            geodetic2aer(Coordinates(s).GEO(k,1),Coordinates(s).GEO(k,2),Coordinates(s).GEO(k,3),...
                                         Object.ReferencePositionGeodetic(1),Object.ReferencePositionGeodetic(2),Object.ReferencePositionGeodetic(3),...
                                         Object.Ellipsoid);
                        
                        % Local spherical coordinates
                        Visibility(s).AER(k,1) = a;
                        Visibility(s).AER(k,2) = e;
                        Visibility(s).AER(k,3) = r;
                        
                        % Visibility
                        Visibility(s).Indicator(k,1) = ge(e,Object.ElevationMaskAngle);
                                                
                    end
                    
                end
                
            end
            
        end
        
        % Method 'calculateDOP'
        function [HDOP, VDOP, PDOP] = calculateDOP(Object,Coordinates,Visibility)
            
            % Number of iterations
            s = 0;
            K = 0;
            while K == 0
                s = s+1;
                K = numel(Visibility(s).Indicator);
            end
            
            % Preallocations
            HDOP = nan(K,1);
            VDOP = nan(K,1);
            PDOP = nan(K,1);
            
            for k = 1:K
                
                % List of visible and healthy SVs
                S = find([Visibility(:).Indicator]);
                
                % Normalized distances matrix
                N = numel(S);
                A = -ones(N,4);
                for n = 1:N
                    A(n,1) = (Coordinates(S(n)).ECEF(k,1)-Object.ReferencePositionECEF(1))/Visibility(S(n)).AER(k,3);
                    A(n,2) = (Coordinates(S(n)).ECEF(k,2)-Object.ReferencePositionECEF(2))/Visibility(S(n)).AER(k,3);
                    A(n,3) = (Coordinates(S(n)).ECEF(k,3)-Object.ReferencePositionECEF(3))/Visibility(S(n)).AER(k,3);
                end
                
                % Covariance matrix
                Q = inv(A'*A);
                
                % DOP
                HDOP2 = Q(1,1)+Q(2,2);
                VDOP2 = Q(3,3);
                HDOP(k,1) = sqrt(HDOP2);
                VDOP(k,1) = sqrt(VDOP2);
                PDOP(k,1) = sqrt(HDOP2+VDOP2);
                
            end
            
        end
        
        % Method 'calculateHorizon' (null elevation and mask elevation)
        function [Horizon, HorizonMask] = calculateHorizon(Object,N)
            
            if nargin == 1
                N = 200;
            end
            Horizon     = nan(N,2);
            HorizonMask = nan(N,2);
            
            % Mean GPS orbit radius
            R = [Object.Almanac(:).sqrtA];
            R = R(~isnan(R));
            R = R*R'/numel(R);
            
            % Distance to GPS orbit along horizon
            r = sqrt(R^2-norm(Object.ReferencePositionECEF));
            
            % Coordinates of horizon projection on Earth
            theta = linspace(0,2*pi,N);                       
            for n = 1:N       
                ge = r*sin(theta(n));
                gn = r*cos(theta(n));
                [l,g,~] = ...
                    enu2geodetic(ge,gn,0,...
                                 Object.ReferencePositionGeodetic(1),Object.ReferencePositionGeodetic(2),Object.ReferencePositionGeodetic(3),...
                                 Object.Ellipsoid);
             	Horizon(n,1) = l;
                Horizon(n,2) = g;   
              	[l,g,~] = ...
                    enu2geodetic(ge*cos(Object.Constants.Mathematics.deg2rad*Object.ElevationMaskAngle),gn*cos(Object.Constants.Mathematics.deg2rad*Object.ElevationMaskAngle),r*sin(Object.Constants.Mathematics.deg2rad*Object.ElevationMaskAngle),...
                                 Object.ReferencePositionGeodetic(1),Object.ReferencePositionGeodetic(2),Object.ReferencePositionGeodetic(3),...
                                 Object.Ellipsoid);
             	HorizonMask(n,1) = l;
                HorizonMask(n,2) = g;
            end
            
            % Longitude discontinuity correction
            Horizon     = LongitudeDiscontinuityCorrection(Horizon);
            HorizonMask = LongitudeDiscontinuityCorrection(HorizonMask);
            
            % Longitude discontinuity correction
            function H2 = LongitudeDiscontinuityCorrection(H1)
                
                % Complete data
                H1 = [H1; H1(1,:)];
                
                % Index of corrected coordinates
                n2 = 0;
                
                for n1 = 1:size(H1,1)-1
                    
                    % Index increment
                    n2 = n2+1;
                    
                    % Consecutive longitude difference
                    d = diff(H1([n1 n1+1],2));
                    
                    % Copy
                    H2(n2,1) = H1(n1,1); %#ok<AGROW>
                    H2(n2,2) = H1(n1,2); %#ok<AGROW>
                    
                    if abs(d) > 180
                        
                        % Last point of the current segment
                        n2 = n2+1;
                        H2(n2,1) = H1(n1+1,1); %#ok<AGROW>
                        H2(n2,2) = H1(n1+1,2)-sign(d)*360; %#ok<AGROW>
                        n2 = n2+1;
                        
                        % Discontinity between two consecutive segments
                        H2(n2,1) = NaN; %#ok<AGROW>
                        H2(n2,2) = NaN; %#ok<AGROW>
                        
                        % First point of the next segment
                        n2 = n2+1;
                        
                        % First point of the current index
                        H2(n2,1) = H1(n1,1); %#ok<AGROW>
                        H2(n2,2) = H1(n1,2)+sign(d)*360; %#ok<AGROW>
                        
                    end
                    
                end
                
            end
            
        end
        
    end
    
end
