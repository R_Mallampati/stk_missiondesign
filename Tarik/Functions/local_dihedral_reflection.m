%% Local dihedral reflection
%Author: Tarik Errabih

%Description: This function calculates in the ship's local frame the
%dihedral reflection occuring at altitude 'planeAlt' [m] when a GPS satellite is at elevation
%'satElevation' [°] and azimuth 'satAzimuth' [°]; and the ship is oriented with an
%angle 'shipAngle' [rad] with respect to local North, measured clockwise in radians.

function reflection = local_dihedral_reflection(satElevation, satAzimuth, planeAlt, shipAngle)
    %Conversion to radians
    satElevation = deg2rad(satElevation);
    satAzimuth = deg2rad(satAzimuth);
    
    %Conversion of angles to clockwise-measured angles from true north
    phi = pi/2 - satElevation;
    r = planeAlt/cos(phi);
    
    %Spherical to cartesian coordinates conversion
    X = r*sin(phi)*cos(3*pi/2 + satAzimuth - 2*shipAngle); 
    Y = r*sin(phi)*sin(3*pi/2 + satAzimuth - 2*shipAngle);
    Z = r*cos(phi);
    
    reflection = [X, Y, Z];
end