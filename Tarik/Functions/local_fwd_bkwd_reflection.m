%% Local forward and backward reflections
%Author: Tarik Errabih

%Description: This function calculates in the ship's local frame the
%forward and backward reflection occuring at altitude 'planeAlt' [m] when a GPS satellite is at elevation
%'satElevation' [°] and azimuth 'satAzimuth' [°].

function reflection = local_fwd_bkwd_reflection(satElevation, satAzimuth, planeAlt)
    %Conversion to radians
    satElevation = deg2rad(satElevation);
    satAzimuth = deg2rad(satAzimuth);
    
    %Conversion of angles to clockwise-measured angles from true north
    phi = pi/2 - satElevation;
    theta = pi/2 - satAzimuth;
    r = planeAlt/cos(phi);
    
    %Spherical to cartesian coordinates conversion
    X = r*sin(phi)*cos(theta); 
    Y = r*sin(phi)*sin(theta);
    Z = r*cos(phi);
    
    reflection = [X, Y, Z];
end