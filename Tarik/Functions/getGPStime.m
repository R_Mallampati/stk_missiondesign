%% GPS time
%Author: Tarik Errabih
%Acknowledgment Eric Ogier

%This function takes a 'datetime' Matlab object and converts it into the
%GPS time in seconds since the previous Sunday at 12am). This format is the
%one used in Eric Ogier's functions for computing the GPS constellation.

function t = getGPStime(T)
    %Adding of leap seconds
    T = T + 18/86400;

    %Current date
    DV = datevec(T);

    %Day number
    d = day(T,'dayofweek');

    %GPS time of week [s]
    t = 86400*(d-1)+[3600 60 1]*DV(4:6)';
end