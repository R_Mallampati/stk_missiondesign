clear
cd(fileparts(which(mfilename))); addpath('./Functions'); addpath('./Functions/Score functions');

%% Settings
% Flight Dynamics
minTurnRadius = .9; % [km]
speed = 60; % [m/s]

% Main settings
shipPosLLA = [-33.5, 151.52, 0];                         % [°,°,m]
ElevationMaskAngle = 25;                                 %Elevation Treshold for visibility in deg

almanacDir = './Almanachs/';
almanacFilename = '17-Nov-2020.txt';

CSVdir = './CSV/';
CSVfilename = 'rx.csv';                                  % Flight trajectory file

% Secondary settings
refreshRate = 0.8;                                       % [s]
N = 30;                                                  % Number of reflections to be calculated per satellite
UpdateAlmanac = 0;                                       % 1 to retrieve and use the latest almanach, 0 to use the specified file

% Additional settings
minAlt = 100;                                            % Treshold altitude below which the code stops running automatically [m]
numPointsFlightPath = 120;                               % Number of flight path points to display (last ones). 

%% Initialisation
c = 299792458; % Speed of light, for delay calculation [m/s]
wgs84 = wgs84Ellipsoid;

if UpdateAlmanac;
    URL = ['https://www.navcen.uscg.gov/?pageName=currentAlmanac&format=yuma-txt'];
    almanacFilename = strcat(almanacDir,date,'.txt');
    urlwrite(URL,almanacFilename);
else
    almanacFilename = strcat(almanacDir,almanacFilename);
end

GPS = Constellation();
GPS.set('ReferencePositionGeodetic',shipPosLLA,'ElevationMaskAngle',ElevationMaskAngle);
GPS.readAlmanac(almanacFilename);
clear ans

%% Loop
numLoops = 1;
flying = 1;

while flying
%% Plane trajectory and current altitude
    planeLLA = csvread(strcat(CSVdir, CSVfilename),1);  
    airplaneAlt = planeLLA(end,end); 
    [planeEast, planeNorth, planeAlt] = geodetic2enu(planeLLA(:,1), planeLLA(:,2), planeLLA(:,3), shipPosLLA(1), shipPosLLA(2), shipPosLLA(3), wgs84);
    
    % Movement test : the program will automatically stop when the plane is
    % below minAlt [m]
    if planeLLA(end,3) < minAlt
        flying = 0;
    end
        
    %% Instantaneous position and visibility determination
    t = GPS.getTime;
    OrbitalParameters = GPS.calculateCoordinates(t, 1:32);

    Visibility = GPS.calculateVisibility(OrbitalParameters);
    VisibilityIndicator = [Visibility.Indicator];
    VisibleSVs = nonzeros([1:32].*VisibilityIndicator)';

    AER = [Visibility.AER];
    SVelevations = AER(2:3:end);
    SVazimuths = AER(1:3:end);
    
    delay = nonzeros(SVelevations.*VisibilityIndicator);
    delay = 1000*2*airplaneAlt./(c*sin(deg2rad(delay))); % [ms]
    delay = rmmissing(delay);

    numSat = sum(VisibilityIndicator);
    reflections = zeros(N+1, 3, numSat);
    bckwd = zeros(3,numSat);

    k = 1;
    for index = VisibleSVs
        for j = 0:N
            shipAngle = j*pi/N;
            reflections(j+1,:,k) = local_dihedral_reflection(SVelevations(index), SVazimuths(index), airplaneAlt,shipAngle)';            
        end
        bckwd(:,k)= local_fwd_bkwd_reflection(SVelevations(index), SVazimuths(index), airplaneAlt)';
        k = k + 1;
    end
    
%% Score calculation
    PRN = [VisibleSVs VisibleSVs];
    tStep = 300;
    keypoint = [bckwd,-bckwd];
    keypoint = keypoint(1:2,:);
    planePos = [planeEast(end) planeNorth(end)];
    planeDirection = [planeEast(end)-planeEast(end-100) planeNorth(end)-planeNorth(end-100)];
    
    N = numel(keypoint)/2;
    
    turnRad = zeros(1,N);
    timeReach = zeros(1,N);
    timeRemaining = zeros(1,N);
    reachable = zeros(1,N);

    scores = zeros(1,N);
    
    for index = 1:N
        [turnRad(index),centerPos] = TurningRadius(planePos, planeDirection, keypoint(:,index));
        timeRemaining(index) = VisibilityTime(t,tStep,PRN(index),ElevationMaskAngle,GPS);
        timeReach(index) = TimeToReach(planePos, keypoint(:,index), centerPos, turnRad(index), speed);
        reachable(index) = isReachable(turnRad(index), minTurnRadius, timeReach(index), timeRemaining(index));

        scores(index) = 10000*scoreCalc(turnRad(index),timeRemaining(index),timeReach(index),reachable(index),SVelevations(PRN(index)));
    end

table = [turnRad;
    timeReach;
    timeRemaining;
    reachable;
    scores]
flying = 0
% %% Plot
%     hold on
%     
%     % Plane and ship plot
%     if numel(planeEast) < numPointsFlightPath
%         plot(planeEast/1000, planeNorth/1000,'b','Linewidth',1.5)
%     else
%         plot(planeEast(end-100:end)/1000, planeNorth(end-100:end)/1000,'b','Linewidth',1.5)
%     end
%     plot(0,0,'r*')
%     legends = ["Flight Path","Ship Position"];
%     
%     % Circles plot
%     for m=1:numSat
%         plot(reflections(:,1,m)/1000,reflections(:,2,m)/1000);
%         legends = [legends, strcat("PRN", num2str(VisibleSVs(m)), ", Delay ", num2str(delay(m))," [ms]")];
%     end
%     
%     % Forward and backward scatter points plot
%     bckwd = bckwd/1000;
%     fwd = -bckwd;
%     for m=1:numSat
%         plot(bckwd(1,m),bckwd(2,m),'ok');
%         plot(fwd(1,m),fwd(2,m),'xk');
%     end
%     legends = [legends, "Backward Scatter Reflections", "Forward Scatter Reflections"];
%     
%     % Plane current position
%     plot(planeEast(end)/1000, planeNorth(end)/1000,'bx')
%     
%     %Plot settings
%     f = get(gca,'Children');
%     f = flip(f);
%     legend(f(1:numSat+4),legends)
%     maxRadius = airplaneAlt/tan(deg2rad(ElevationMaskAngle))/1000;    
%     xlim([-1.3*maxRadius 1.3*maxRadius])
%     ylim([-1.3*maxRadius 1.3*maxRadius])
%     xlabel('East left (km)'), ylabel('North up (km)')    
%     title("Reflections of the GPS signal at current altitude (local map)") 
%     axis equal
%     
%     pause(refreshRate)
%     clf
%     numLoops = numLoops + 1;
end
FlightDuration = strcat(num2str(refreshRate*numLoops)," seconds")