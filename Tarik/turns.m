function totalTurns = turns(trip,planeDirection,pos,idxs,nPoints)
    
    edges = idxs(nonzeros(trip),:);
    tripsGraph = graph(edges(:,1),edges(:,2));
    
    degrees = zeros(nPoints,1);
    
    for i = 2:nPoints
        degrees(i) = degree(tripsGraph,i)-2;
    end
    finalNode = find(degrees);
    
    tripOrder = shortestpath(tripsGraph,1,finalNode)
    currentPos = 1;
    cumulatedAngle = 0;
    
    for j = 2:nPoints-1
        nextPos = tripOrder(j);
        xk = pos(1,nextPos);
        yk = pos(2,nextPos);

        xp = pos(1,currentPos);
        yp = pos(2,currentPos);

        a = -planeDirection(2);
        b = planeDirection(1);

        dif = (norm(pos(:,currentPos))^2-norm(pos(:,nextPos))^2)/2;

        xCenter = (dif+(yp-yk)*(b/a*xp+yp))/(xp-xk+b/a*(yp-yk));
        yCenter = b/a*(xCenter-xp)+yp;

        centerPos = [xCenter, yCenter];

        R = sqrt((xp-xCenter)^2+(yp-yCenter)^2);%/1000;
        angle = pi - 2*acos(norm(pos(:,nextPos)-pos(:,currentPos))/(2*R))
        %rotate planedirection
        
        cumulatedAngle = angle + cumulatedAngle;
        currentPos = nextPos;
    end
end