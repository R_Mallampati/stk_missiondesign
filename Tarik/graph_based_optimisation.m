clear
cd(fileparts(which(mfilename))); addpath('./Functions'); addpath('./Functions/Score Functions')

%% Settings
% Main settings
shipPosLLA = [-33.5, 151.52, 0];                         % [°,°,m]
ElevationMaskAngle = 25;                                 %Elevation Treshold for visibility in deg

almanacDir = './Almanachs/';
almanacFilename = '17-Nov-2020.txt';

CSVdir = './CSV/';
CSVfilename = 'rx.csv';                                  % Flight trajectory file

% Secondary settings
UpdateAlmanac = 0;                                       % 1 to retrieve and use the latest almanach, 0 to use the specified file

% Additional settings
minAlt = 100;                                            % Treshold altitude below which the code stops running automatically [m]
numPointsFlightPath = 120;                               % Number of flight path points to display (last ones). 

%% Initialisation
c = 299792458; % Speed of light, for delay calculation [m/s]
wgs84 = wgs84Ellipsoid;

if UpdateAlmanac;
    URL = ['https://www.navcen.uscg.gov/?pageName=currentAlmanac&format=yuma-txt'];
    almanacFilename = strcat(almanacDir,date,'.txt');
    urlwrite(URL,almanacFilename);
else
    almanacFilename = strcat(almanacDir,almanacFilename);
end

GPS = Constellation();
GPS.set('ReferencePositionGeodetic',shipPosLLA,'ElevationMaskAngle',ElevationMaskAngle);
GPS.readAlmanac(almanacFilename);
clear ans

%% Plane trajectory and current altitude
planeLLA = csvread(strcat(CSVdir, CSVfilename),1);  
airplaneAlt = planeLLA(end,end); 
[planeEast, planeNorth, planeAlt] = geodetic2enu(planeLLA(:,1), planeLLA(:,2), planeLLA(:,3), shipPosLLA(1), shipPosLLA(2), shipPosLLA(3), wgs84);

planePos = [planeEast(end); planeNorth(end)]/1000;
planeDirection = [planeEast(end)-planeEast(end-100) planeNorth(end)-planeNorth(end-100)];
planeDirection = planeDirection/norm(planeDirection);

% Movement test : the program will automatically stop when the plane is
% below minAlt [m]
if planeLLA(end,3) < minAlt
    moving = 0;
end

%% Instantaneous position and visibility determination
t = GPS.getTime;
OrbitalParameters = GPS.calculateCoordinates(t, 1:32);

Visibility = GPS.calculateVisibility(OrbitalParameters);
VisibilityIndicator = [Visibility.Indicator];
VisibleSVs = nonzeros([1:32].*VisibilityIndicator)';

AER = [Visibility.AER];
SVelevations = AER(2:3:end);
SVazimuths = AER(1:3:end);

delay = nonzeros(SVelevations.*VisibilityIndicator);
delay = 1000*2*airplaneAlt./(c*sin(deg2rad(delay))); % [ms]
delay = rmmissing(delay);

numSat = sum(VisibilityIndicator);
bckwd = zeros(3,numSat);

k = 1;
for i = VisibleSVs
    bckwd(:,k)= local_fwd_bkwd_reflection(SVelevations(i), SVazimuths(i), airplaneAlt)';
    k = k + 1;
end
bckwd = bckwd/1000;
fwd = -bckwd;

%% Graph generation
pos = [planePos bckwd(1:2,:)];
nPoints = size(pos,2);

idxs = nchoosek(1:nPoints,2);
dist = hypot(pos(idxs(:,1)) - pos(idxs(:,2)), ...
             pos(idxs(:,1)) - pos(idxs(:,2)));
lendist = length(dist);

nodename = {'Plane'};
for i=1:nPoints-1
    nodename = horzcat(nodename,strcat('SV ',num2str(VisibleSVs(i))));
end
G = graph(idxs(:,1),idxs(:,2));

%% Problem definition
tsp = optimproblem;
trips = optimvar('trips',lendist,1,'Type','integer','LowerBound',0,'UpperBound',1);
tsp.Objective = dist'*trips;

%% Constraints
constr2trips = optimconstr(2,1);
whichIdxs = outedges(G,1); % Identify trips associated with the stop
constr2trips(1) = sum(trips(whichIdxs)) == 1;

constr2triplength = optimconstr(nPoints,1);
totalEdges = sum(trips);
constr2trips(2) = totalEdges == nPoints-1;

for stop = 2:nPoints
    whichIdxs = outedges(G,stop); % Identify trips associated with the stop
    constr2triplength(stop) = sum(trips(whichIdxs)) <= 2;
end

tsp.Constraints.constr2trips = constr2trips;
tsp.Constraints.constr2triphaha = constr2triplength;
opts = optimoptions('intlinprog','Display','off');
tspsol = solve(tsp,'options',opts);

tspsol.trips = logical(round(tspsol.trips));
Gsol = graph(idxs(tspsol.trips,1),idxs(tspsol.trips,2),[],nodename);

%% Removing subtours
tourIdxs = conncomp(Gsol);
numtours = max(tourIdxs); % Number of subtours
fprintf('# of subtours: %d\n',numtours);


% Index of added constraints for subtours
k = 1;
while numtours > 1 % Repeat until there is just one subtour
    % Add the subtour constraints
    for ii = 1:numtours
        inSubTour = (tourIdxs == ii); % Edges in current subtour
        a = all(inSubTour(idxs),2); % Complete graph indices with both ends in subtour
        constrname = "subtourconstr" + num2str(k);
        tsp.Constraints.(constrname) = sum(trips(a)) <= (nnz(inSubTour) - 1);
        k = k + 1;        
    end
    
    % Try to optimize again
    [tspsol,fval,exitflag,output] = solve(tsp,'options',opts);
    tspsol.trips = logical(round(tspsol.trips));
    Gsol = graph(idxs(tspsol.trips,1),idxs(tspsol.trips,2),[],nodename);
    
    % How many subtours this time?
    tourIdxs = conncomp(Gsol);
    numtours = max(tourIdxs); % Number of subtours
    fprintf('# of subtours: %d\n',numtours)    
end
plot(Gsol)