stk.v.11.0
WrittenBy    STK_v11.6.0

BEGIN Ship

    Name		 Ship3

    BEGIN VehiclePath
        CentralBody		 Earth

        BEGIN Interval

            StartTime		 21 Oct 2020 01:00:00.000000000
            StopTime		 14 Nov 2041 14:12:19.222253680

        END Interval

        StoreEphemeris		 Yes
        SmoothInterp		 No

        BEGIN GreatArc

            VersionIndicator		 20071204
            Method		 DetTimeAccFromVel

            ArcSmartInterval		
            BEGIN EVENTINTERVAL
                StartEvent		
                BEGIN EVENT
                    Epoch		 21 Oct 2020 01:00:00.000000000
                    EventEpoch		
                    BEGIN EVENT
                        Type		 EVENT_LINKTO
                        Name		 AnalysisStartTime
                        AbsolutePath		 Scenario
                    END EVENT
                    EpochState		 Implicit
                END EVENT
                StopEvent		
                BEGIN EVENT
                    Epoch		 14 Nov 2041 14:12:19.222253680
                    EpochState		 Explicit
                END EVENT
                IntervalState		 StartStop
            END EVENTINTERVAL

            TimeOfFirstWaypoint		 21 Oct 2020 01:00:00.000000000

            UseScenTime		 No
            ArcGranularity		  8.9992440604777096e-03
            DefaultRate		  1.5433332999999999e+01
            DefaultAltitude		  0.0000000000000000e+00
            DefaultTurnRadius		  0.0000000000000000e+00
            AltRef		 MSL
            AltInterpMethod		 MSLHeight
            NumberOfWaypoints		 2

            BEGIN Waypoints

                 0.0000000000000000e+00 -3.3899106719999999e+01  1.5138073843999999e+02  0.0000000000000000e+00  1.0000000000000001e-05  0.0000000000000000e+00
                 6.6480913922225368e+08 -3.3853839000000001e+01  1.5142783516000000e+02  0.0000000000000000e+00  1.0000000000000001e-05  0.0000000000000000e+00

            END Waypoints

        END GreatArc

    END VehiclePath

    BEGIN Ephemeris

        NumberOfEphemerisPoints		 8

        ScenarioEpoch		 21 Oct 2020 01:00:00.000000

# Epoch in JDate format: 2459143.54166666666667
# Epoch in YYDDD format:   20295.04166666666667


        InterpolationMethod		 GreatArcMSL

        InterpolationSamplesM1		 1

        CentralBody		 Earth

        CoordinateSystem		 Fixed

        BlockingFactor		 20

# Time of first point: 21 Oct 2020 01:00:00.000000000 UTCG = 2459143.54166666666667 JDate = 20295.04166666666667 YYDDD

        EphemerisTimePosVel		

 0.0000000000000000e+00 -4.6520408228849806e+06  2.5384041710719489e+06 -3.5371752907802118e+06 -6.8372850444434464e-06 -3.7376328794558845e-06  6.2675061770564120e-06
 9.4972662156653598e+07 -4.6526901323233293e+06  2.5380491706487234e+06 -3.5365800020121075e+06 -6.8362039248626337e-06 -3.7382253473645166e-06  6.2683320867687124e-06
 1.8994533540415764e+08 -4.6533393391538877e+06  2.5376941139188129e+06 -3.5359846347398530e+06 -6.8351226526840871e-06 -3.7388177340267665e-06  6.2691578600711449e-06
 2.8491801974225920e+08 -4.6539884433622118e+06  2.5373390008898838e+06 -3.5353891889763251e+06 -6.8340412280147868e-06 -3.7394100394871403e-06  6.2699834968185468e-06
 3.7989071517142385e+08 -4.6546374449338727e+06  2.5369838315695897e+06 -3.5347936647344003e+06 -6.8329596507339646e-06 -3.7400022637360776e-06  6.2708089971462695e-06
 4.7486342169107652e+08 -4.6552863438544190e+06  2.5366286059655962e+06 -3.5341980620269612e+06 -6.8318779210272656e-06 -3.7405944066594046e-06  6.2716343609180985e-06
 5.6983613930184567e+08 -4.6559351401094291e+06  2.5362733240855690e+06 -3.5336023808668861e+06 -6.8307960388183585e-06 -3.7411864683347300e-06  6.2724595881689458e-06
 6.6480913922225368e+08 -4.6565838355368283e+06  2.5359179849223327e+06 -3.5330066195656024e+06 -6.8297140018042810e-06 -3.7417784384701162e-06  6.2732846875427802e-06


    END Ephemeris

    BEGIN MassProperties

        Mass		  1.0000000000000000e+03
        InertiaXX		  4.5000000000000000e+03
        InertiaYX		  0.0000000000000000e+00
        InertiaYY		  4.5000000000000000e+03
        InertiaZX		  0.0000000000000000e+00
        InertiaZY		  0.0000000000000000e+00
        InertiaZZ		  4.5000000000000000e+03

    END MassProperties

    BEGIN Attitude

        ScenarioEpoch		 21 Oct 2020 01:00:00.000000

        BEGIN Profile
            Name		 AircraftAtt
            UserNamed		 No
            StartTime		  0.0000000000000000e+00
            BEGIN ECFVelRadial
                Azimuth		  0.0000000000000000e+00
            END ECFVelRadial
        END Profile

    END Attitude

    BEGIN Swath

        SwathType		 ElevAngle
        ElevationAngle		  0.0000000000000000e+00
        HalfAngle		  0.0000000000000000e+00
        Distance		  0.0000000000000000e+00
        RepType		 NoSwath

    END Swath

    BEGIN Eclipse

        Sunlight		 Off
        SunlightLineStyle		 0
        SunlightLineWidth		 3
        SunlightMarkerStyle		 19

        Penumbra		 Off
        PenumbraLineStyle		 0
        PenumbraLineWidth		 3
        PenumbraMarkerStyle		 19

        Umbra		 Off
        UmbraLineStyle		 0
        UmbraLineWidth		 3
        UmbraMarkerStyle		 19

        SunlightPenumbraLine		 Off
        PenumbraUmbraLine		 Off

        SunlightColor		 #ffff00
        PenumbraColor		 #87cefa
        UmbraColor		 #0000ff
        UseCustomEclipseBodies		 No

    END Eclipse

    BEGIN RealTimeDef

        HistoryDuration		  1.8000000000000000e+03
        LookAheadDuration		  1.8000000000000000e+03

    END RealTimeDef


    BEGIN LineOfSightModel

        ModelType		 CbShape
        HeightAboveSurface		  0.0000000000000000e+00

    END LineOfSightModel


    BEGIN Extensions

        BEGIN LaserCAT
        END LaserCAT

        BEGIN ExternData
        END ExternData

        BEGIN RFI
        END RFI

        BEGIN ADFFileData
        END ADFFileData

        BEGIN AccessConstraints
            LineOfSight IncludeIntervals
        END AccessConstraints

        BEGIN ObjectCoverage
        END ObjectCoverage

        BEGIN Desc
        END Desc

        BEGIN Atmosphere
<?xml version = "1.0" standalone = "yes"?>
<VAR name = "STK_Atmosphere_Extension">
    <SCOPE Class = "AtmosphereExtension">
        <VAR name = "Version">
            <STRING>&quot;1.0.0 a&quot;</STRING>
        </VAR>
        <VAR name = "STKVersion">
            <INT>1160</INT>
        </VAR>
        <VAR name = "ComponentName">
            <STRING>&quot;STK_Atmosphere_Extension&quot;</STRING>
        </VAR>
        <VAR name = "Description">
            <STRING>&quot;STK Atmosphere Extension&quot;</STRING>
        </VAR>
        <VAR name = "Type">
            <STRING>&quot;STK Atmosphere Extension&quot;</STRING>
        </VAR>
        <VAR name = "UserComment">
            <STRING>&quot;STK Atmosphere Extension&quot;</STRING>
        </VAR>
        <VAR name = "ReadOnly">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "Clonable">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "Category">
            <STRING>&quot;&quot;</STRING>
        </VAR>
        <VAR name = "InheritAtmosAbsorptionModel">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "AtmosAbsorptionModel">
            <VAR name = "Simple_Satcom">
                <SCOPE Class = "AtmosphericAbsorptionModel">
                    <VAR name = "Version">
                        <STRING>&quot;1.0.1 a&quot;</STRING>
                    </VAR>
                    <VAR name = "STKVersion">
                        <INT>1160</INT>
                    </VAR>
                    <VAR name = "ComponentName">
                        <STRING>&quot;Simple_Satcom&quot;</STRING>
                    </VAR>
                    <VAR name = "Description">
                        <STRING>&quot;Simple Satcom gaseous absorption model&quot;</STRING>
                    </VAR>
                    <VAR name = "Type">
                        <STRING>&quot;Simple Satcom&quot;</STRING>
                    </VAR>
                    <VAR name = "UserComment">
                        <STRING>&quot;Simple Satcom gaseous absorption model&quot;</STRING>
                    </VAR>
                    <VAR name = "ReadOnly">
                        <BOOL>false</BOOL>
                    </VAR>
                    <VAR name = "Clonable">
                        <BOOL>true</BOOL>
                    </VAR>
                    <VAR name = "Category">
                        <STRING>&quot;&quot;</STRING>
                    </VAR>
                    <VAR name = "SurfaceTemperature">
                        <QUANTITY Dimension = "Temperature" Unit = "K">
                            <REAL>293.15</REAL>
                        </QUANTITY>
                    </VAR>
                    <VAR name = "WaterVaporConcentration">
                        <QUANTITY Dimension = "Density" Unit = "g*m^-3">
                            <REAL>7.5</REAL>
                        </QUANTITY>
                    </VAR>
                </SCOPE>
            </VAR>
        </VAR>
        <VAR name = "EnableLocalRainData">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "LocalRainIsoHeight">
            <QUANTITY Dimension = "DistanceUnit" Unit = "m">
                <REAL>2000</REAL>
            </QUANTITY>
        </VAR>
        <VAR name = "LocalRainRate">
            <QUANTITY Dimension = "SlowRate" Unit = "mm*hr^-1">
                <REAL>1</REAL>
            </QUANTITY>
        </VAR>
        <VAR name = "LocalSurfaceTemp">
            <QUANTITY Dimension = "Temperature" Unit = "K">
                <REAL>293.15</REAL>
            </QUANTITY>
        </VAR>
    </SCOPE>
</VAR>        END Atmosphere

        BEGIN RadarCrossSection
<?xml version = "1.0" standalone = "yes"?>
<VAR name = "STK_Radar_RCS_Extension">
    <SCOPE Class = "RadarRCSExtension">
        <VAR name = "Version">
            <STRING>&quot;1.0.0 a&quot;</STRING>
        </VAR>
        <VAR name = "STKVersion">
            <INT>1160</INT>
        </VAR>
        <VAR name = "ComponentName">
            <STRING>&quot;STK_Radar_RCS_Extension&quot;</STRING>
        </VAR>
        <VAR name = "Description">
            <STRING>&quot;STK Radar RCS Extension&quot;</STRING>
        </VAR>
        <VAR name = "Type">
            <STRING>&quot;STK Radar RCS Extension&quot;</STRING>
        </VAR>
        <VAR name = "UserComment">
            <STRING>&quot;STK Radar RCS Extension&quot;</STRING>
        </VAR>
        <VAR name = "ReadOnly">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "Clonable">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "Category">
            <STRING>&quot;&quot;</STRING>
        </VAR>
        <VAR name = "Inherit">
            <BOOL>true</BOOL>
        </VAR>
    </SCOPE>
</VAR>        END RadarCrossSection

        BEGIN RadarClutter
<?xml version = "1.0" standalone = "yes"?>
<VAR name = "STK_Radar_Clutter_Extension">
    <SCOPE Class = "RadarClutterExtension">
        <VAR name = "Version">
            <STRING>&quot;1.0.0 a&quot;</STRING>
        </VAR>
        <VAR name = "STKVersion">
            <INT>1160</INT>
        </VAR>
        <VAR name = "ComponentName">
            <STRING>&quot;STK_Radar_Clutter_Extension&quot;</STRING>
        </VAR>
        <VAR name = "Description">
            <STRING>&quot;STK Radar Clutter Extension&quot;</STRING>
        </VAR>
        <VAR name = "Type">
            <STRING>&quot;STK Radar Clutter Extension&quot;</STRING>
        </VAR>
        <VAR name = "UserComment">
            <STRING>&quot;STK Radar Clutter Extension&quot;</STRING>
        </VAR>
        <VAR name = "ReadOnly">
            <BOOL>false</BOOL>
        </VAR>
        <VAR name = "Clonable">
            <BOOL>true</BOOL>
        </VAR>
        <VAR name = "Category">
            <STRING>&quot;&quot;</STRING>
        </VAR>
        <VAR name = "Inherit">
            <BOOL>true</BOOL>
        </VAR>
    </SCOPE>
</VAR>        END RadarClutter

        BEGIN Ellipse
            TimesTrackVehStartTime		 Yes
        END Ellipse

        BEGIN Crdn
        END Crdn

        BEGIN Graphics

            BEGIN GenericGraphics
                IntvlHideShowAll		 Off
                ShowPassLabel		 Off
                ShowPathLabel		 Off
                TransformTrajectory		 On
                MinGfxGndTrkTimeStep		  0.0000000000000000e+00
                MaxGfxGndTrkTimeStep		  6.0000000000000000e+02
                MinGfxOrbitTimeStep		  0.0000000000000000e+00
                MaxGfxOrbitTimeStep		  6.0000000000000000e+02
                ShowGlintPoint		 Off
                ShowGlintColor		 #ffffff
                ShowGlintStyle		 2
            END GenericGraphics

            BEGIN AttributeData
                ShowGfx		 Off
                AttributeType		                Basic		
                BEGIN DefaultAttributes
                    Show		 Off
                    Inherit		 On
                    ShowLabel		 On
                    ShowGndTrack		 On
                    ShowGndMarker		 On
                    ShowOrbit		 On
                    ShowOrbitMarker		 On
                    ShowElsetNum		 Off
                    ShowSpecialSwath		 On
                    MarkerColor		 #00ffff
                    GroundTrackColor		 #00ffff
                    SwathColor		 #00ffff
                    LabelColor		 #00ffff
                    LineStyle		 0
                    LineWidth		 1
                    MarkerStyle		 19
                    FontStyle		 0
                    SwathLineStyle		 0
                    SpecSwathLineStyle		 1
                END DefaultAttributes

                BEGIN CustomIntervalList
                    BEGIN DefaultAttributes
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ffff
                        GroundTrackColor		 #00ffff
                        SwathColor		 #00ffff
                        LabelColor		 #00ffff
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 19
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END DefaultAttributes
                END CustomIntervalList

                BEGIN AccessIntervalsAttributes
                    BEGIN AttrDuringAccess
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ffff
                        GroundTrackColor		 #00ffff
                        SwathColor		 #00ffff
                        LabelColor		 #00ffff
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END AttrDuringAccess
                    BEGIN AttrDuringNoAccess
                        Show		 Off
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #ff00ff
                        GroundTrackColor		 #ff00ff
                        SwathColor		 #ff00ff
                        LabelColor		 #ff00ff
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END AttrDuringNoAccess
                END AccessIntervalsAttributes

                BEGIN TimeComponentIntervalsAttributes
                    BEGIN DefaultAttributes
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ffff
                        GroundTrackColor		 #00ffff
                        SwathColor		 #00ffff
                        LabelColor		 #00ffff
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END DefaultAttributes
                END TimeComponentIntervalsAttributes

                BEGIN RealTimeAttributes
                    BEGIN HistoryAttr
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #00ffff
                        GroundTrackColor		 #00ffff
                        SwathColor		 #00ffff
                        LabelColor		 #00ffff
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END HistoryAttr
                    BEGIN SplineAttr
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #ffff00
                        GroundTrackColor		 #ffff00
                        SwathColor		 #ffff00
                        LabelColor		 #ffff00
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END SplineAttr
                    BEGIN LookAheadAttr
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #ffffff
                        GroundTrackColor		 #ffffff
                        SwathColor		 #ffffff
                        LabelColor		 #ffffff
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END LookAheadAttr
                    BEGIN DropOutAttr
                        Show		 On
                        Inherit		 On
                        ShowLabel		 On
                        ShowGndTrack		 On
                        ShowGndMarker		 On
                        ShowOrbit		 On
                        ShowOrbitMarker		 On
                        ShowElsetNum		 Off
                        ShowSpecialSwath		 On
                        MarkerColor		 #ff0000
                        GroundTrackColor		 #ff0000
                        SwathColor		 #ff0000
                        LabelColor		 #ff0000
                        LineStyle		 0
                        LineWidth		 1
                        MarkerStyle		 4
                        FontStyle		 0
                        SwathLineStyle		 0
                        SpecSwathLineStyle		 1
                    END DropOutAttr
                END RealTimeAttributes
            END AttributeData

            BEGIN LeadTrailData
                GtLeadingType		 AllData
                GtTrailingType		 AllData
                OrbitLeadingType		 AllData
                OrbitTrailingType		 OnePass
            END LeadTrailData
            BEGIN SaaData
                ShowSaa		 Off
                ShowSaaFill		 Off
                SaaFillTranslucency		 0.7
                TrackSaa		 On
                SaaAltitude		 500000
            END SaaData
            BEGIN GroundTracks
                BEGIN GroundTrack
                    CentralBody		 Earth
                END GroundTrack
            END GroundTracks
            BEGIN WaypointData
                ShowWayptMarkers		 On
                ShowWayptTurnMarkers		 On
                ShowWayptMarkersExtEphem		 Off
                NewWayptMarkerShow		 On
                NewWayptShowLabel		 Off
                NewWayptMarkerStyle		 3
                WayptShow		 On
                WayptShowLabel		 Off
                WayptMarkerStyle		 3
            END WaypointData
            BEGIN EllipseSetGxData
                BEGIN DefEllipseSetGx
                    ShowStatic		 On
                    ShowDynamic		 On
                    UseLastDynPos		 Off
                    HoldLastDynPos		 Off
                    ShowName		 Off
                    ShowMarker		 On
                    MarkerStyle		 0
                    LineStyle		 0
                    LineWidth		 1
                END DefEllipseSetGx
            END EllipseSetGxData
        END Graphics

        BEGIN ContourGfx
            ShowContours		 Off
        END ContourGfx

        BEGIN Contours
            ActiveContourType		 Radar Cross Section

            BEGIN ContourSet Radar Cross Section
                Altitude		 0
                ShowAtAltitude		 Off
                Projected		 On
                Relative		 On
                ShowLabels		 Off
                LineWidth		 1
                DecimalDigits		 1
                ColorRamp		 On
                ColorRampStartColor		 #ff0000
                ColorRampEndColor		 #0000ff
                BEGIN ContourDefinition
                    BEGIN CntrAntAzEl
                        CoordinateSystem		 0
                        BEGIN AzElPatternDef
                            SetResolutionTogether		 0
                            NumAzPoints		 361
                            AzimuthRes		 1
                            MinAzimuth		 -180
                            MaxAzimuth		 180
                            NumElPoints		 91
                            ElevationRes		 1
                            MinElevation		 0
                            MaxElevation		 90
                        END AzElPatternDef
                    END CntrAntAzEl
                    BEGIN RCSContour
                        Frequency		 2997924580
                        ComputeType		 0
                    END RCSContour
                END ContourDefinition
            END ContourSet
        END Contours

        BEGIN VO
        END VO

        BEGIN 3dVolume
            ActiveVolumeType		 Radar Cross Section

            BEGIN VolumeSet Radar Cross Section
                Scale		 100
                MinimumDisplayedRcs		 1
                Frequency		  1.4500000000000000e+10
                ShowAsWireframe		 0
                BEGIN AzElPatternDef
                    SetResolutionTogether		 0
                    NumAzPoints		 50
                    AzimuthRes		 7.346938775510203
                    MinAzimuth		 -180
                    MaxAzimuth		 180
                    NumElPoints		 50
                    ElevationRes		 3.673469387755102
                    MinElevation		 0
                    MaxElevation		 180
                END AzElPatternDef
                ColorMethod		 1
                MinToMaxStartColor		 #ff0000
                MinToMaxStopColor		 #0000ff
                RelativeToMaximum		 0
            END VolumeSet
            BEGIN VolumeGraphics
                ShowContours		 No
                ShowVolume		 No
            END VolumeGraphics
        END 3dVolume

    END Extensions

    BEGIN SubObjects

    END SubObjects

END Ship

